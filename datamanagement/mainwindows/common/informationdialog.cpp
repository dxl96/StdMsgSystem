#include "informationdialog.h"
#include "ui_informationdialog.h"

//------------------初始化唯一标识和提示信息窗体的映射-----------------------
QMap<QString, informationDialog*> informationDialog::uniFlags = QMap<QString, informationDialog*>();

informationDialog::informationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::informationDialog)
{
    ui->setupUi(this);
    // 去掉问号，设置最小化、最大化、关闭
    setWindowFlags(Qt::Dialog | Qt::WindowMinimizeButtonHint | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint);
    setWindowTitle("提示信息");

    // 设置文本输入框只读
    ui->information_textEdit->setReadOnly(true);

    // 利用uuid生成唯一标识
    QString uuidUniFlag = QUuid::createUuid().toString().remove("{").remove("}").remove("-");
    // 给当前窗体设置默认唯一编号
    setUniFlag(uuidUniFlag);
}

informationDialog::~informationDialog()
{
    // 从标识集合中移除当前提示窗体的唯一标识
    informationDialog::uniFlags.remove(this->uniFlag);
    delete ui;
}

// -----------------创建提示信息窗体------------------------------------
informationDialog* informationDialog::create(QString uniFlag, QWidget *parent)
{
    if(!ObjectUtil::isEmpty(uniFlag)) {
        if (!informationDialog::uniFlags.contains(uniFlag)) {
            // 打开信息提示窗口
            informationDialog* inforWidow =  new informationDialog(parent);
            inforWidow->setAttribute(Qt::WA_DeleteOnClose); // 关闭时销毁对象
            inforWidow->setModal(false); // 设置为非模态窗口
            // 重置唯一标识
            inforWidow->setUniFlag(uniFlag);
            return inforWidow;
        } else {
            return informationDialog::uniFlags.value(uniFlag);
        }
    } else {
        informationDialog* inforWidow =  new informationDialog(parent);
        inforWidow->setAttribute(Qt::WA_DeleteOnClose); // 关闭时销毁对象
        inforWidow->setModal(false); // 设置为非模态窗口
        return inforWidow;
    }
}

//-------------------------设置标题---------------------------
void informationDialog::setTitle(QString title)
{
    setWindowTitle(title);
}

//-------------------------提示信息---------------------------
void informationDialog::setInfo(QString info)
{
    // 设置文本输入框内容
    ui->information_textEdit->setText(info);
}

void informationDialog::setUniFlag(QString uniFlag)
{
    qInfo()<< "前置,设置提示信息窗体唯一标识:" << uniFlag;
    // 如果设置的唯一标识在标识集合中不存在，则可以设置
    if (!ObjectUtil::isEmpty(uniFlag)) {
        if (!informationDialog::uniFlags.contains(uniFlag)) {
            // 删除当前提示窗体在标识集合中老的标识
            if (!ObjectUtil::isEmpty(this->uniFlag) && informationDialog::uniFlags.contains(this->uniFlag)) {
                qInfo()<<"中置,删除提示信息窗体唯一标识:"<<this->uniFlag<<",添加新的提示信息窗体唯一标识:"<<uniFlag;
                informationDialog::uniFlags.remove(this->uniFlag);
            }
            // 设置新的标识
            this->uniFlag = uniFlag;
            // 添加当前提示窗体的标识到集合
            informationDialog::uniFlags.insert(uniFlag, this);
            qInfo()<< "后置,成功设置提示信息窗体唯一标识:" << uniFlag;
        } else {
            qCritical()<< "后置,失败设置提示信息窗体唯一标识:" << uniFlag << "预设值唯一标识已经存在;当前窗体的唯一标识目前为:"<<uniFlag;
        }
    } else{
        qCritical()<< "后置,失败设置提示信息窗体唯一标识:" << uniFlag << ",预设值唯一标识为空，不符合要求";
    }
}
