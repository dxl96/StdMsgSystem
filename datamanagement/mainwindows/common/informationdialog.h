#ifndef INFORMATIONDIALOG_H
#define INFORMATIONDIALOG_H

#include <QDialog>
#include <QtWidgets>
#include <QUuid>
#include "datamanagement/userdefine/userdefine.h"

namespace Ui {
class informationDialog;
}

class informationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit informationDialog(QWidget *parent = 0);
    ~informationDialog();

    /**
     * @brief create 创建提示信息窗体
     * @param uniFlag 唯一标识
     * @param parent 父窗体
     * @return 提示信息窗体
     */
    static informationDialog *create(QString uniFlag,QWidget *parent = 0);

    /**
     * @brief setTitle 设置提示信标题
     * @param title 标题
     */
    void setTitle(QString title);

    /**
     * @brief setInfo 设置提示信息
     * @param info 提示信息
     */
    void setInfo(QString info);

    /**
     * @brief setUniFlag 设置该窗体对应的唯一编号
     * @param uniFlag 唯一编号
     */
    void setUniFlag(QString uniFlag);
private:
    /**
     * @brief UNI_FLAGS 所有的提示信息窗体唯一编号和提示窗体的映射
     */
    static QMap<QString, informationDialog*> uniFlags;
    /**
     * @brief ui ui对象
     */
    Ui::informationDialog *ui;
    /**
     * @brief UNI_FLAGS 本提示信息窗体唯一编号
     */
    QString uniFlag;
};

#endif // INFORMATIONDIALOG_H
