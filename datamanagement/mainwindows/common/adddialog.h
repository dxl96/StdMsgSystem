#ifndef ADDDIALOG_H
#define ADDDIALOG_H

#include <QDialog>
#include <QLineEdit>
#include <QtConcurrent/QtConcurrent>

namespace Ui {
class addDialog;
}

class addDialog : public QDialog
{
    Q_OBJECT

public:
    explicit addDialog(QWidget *parent = 0);
    ~addDialog();

private slots:
    void on_submit_pushButton_clicked();

private:
    Ui::addDialog *ui;
    QMap<int, QLineEdit*> inputTextMap;
    void userDefinedlayout();
};

#endif // ADDDIALOG_H
