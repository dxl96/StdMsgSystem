#ifndef COMMONFORM_H
#define COMMONFORM_H

#include <QWidget>
#include <QString>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QtWidgets>
#include <QTimer>
#include "datamanagement/userdefine/userdefine.h"
#include "updatedialog.h"
#include "adddialog.h"
#include "informationdialog.h"
#include "datamanagement/columnsetting/columnsettingdialog.h"

namespace Ui {
class CommonForm;
}

class CommonForm : public QWidget
{
    Q_OBJECT

public:
    explicit CommonForm(QWidget *parent = 0);

    ~CommonForm();

    // -------------------------------------------------------当前对象对应的内部的公共函数，供外部对该对象调用设置-------------------------------
    /**
     * @brief setTable 设置当前对象对应的表信息
     * @param table 表信息对象
     */
    void setTable(UserDefineTableEntity table);
    /**
     * @brief getTable 获得当前对象对应的表信息
     * @return
     */
    UserDefineTableEntity getTable();
    /**
     * @brief loadView 加载界面信息
     */
    void loadView();
    /**
     * @brief loadView 加载界面信息
     * @param table 当前对象对应的表信息
     */
    void loadView(UserDefineTableEntity table);

    /**
     * @brief userdefineTableReflash  刷新界面表格数据
     * @param page 当前页码
     * @param size 单页大小
     * @param conditions 查询条件
     * @return 数据库的对应表总数据量
     */
    int userdefineTableReflash(int page, int size, QList<QueryConditions> conditions);

    /**
     * @brief autoUserdefineTableReflash 根据界面条件刷新界面表格数据
     * @param isStart 是否从开始第一页刷新，如果为true,则从第一页刷新数据，为false,则读取页面当前页码标签，从界面显示的当前页码刷新数据
     */
    void autoUserdefineTableReflash(bool isStart);

    /**
     * @brief initUserdefineTable 初始化数据到界面表格
     */
    void initUserdefineTable();

    /**
     * @brief updateUserdefineQTableWidgetHeader 更新界面表格列表头显示
     */
    void updateUserdefineQTableWidgetHeader();

    /**
     * @brief getCurrentObjectAllColumn 获得当前对象中的table（对应数据库表）的字段集合
     */
     QList<ColumnEntity> getCurrentObjectAllColumn();

    /**
     * @brief updateSelectColumnCombobox 更新条件查询下拉列表的数据信息
     */
    void updateSelectColumnCombobox();

    /**
     * @brief updateColumnHideShowCombobox 更新列显示隐藏下拉列表的数据信息
     */
    void updateColumnHideShowCombobox();

    /**
     * @brief autoAdjustmentTableWidth 自动界面调整表格列宽
     */
    void autoAdjustmentTableWidth();

    /**
     * @brief getUi 获得该界面ui对象
     * @return
     */
    Ui::CommonForm* getUi();

private slots:
    //-----------------------------------------------由界面转到槽的函数----------------------------
    // 修改信息点击槽函数
    void on_change_pushButton_clicked();

    // 新增信息点击槽函数
    void on_insert_pushButton_clicked();

    // 删除信息点击槽函数
    void on_delete_pushButton_clicked();

    // 查找信息点击槽函数
    void on_refer_pushButton_clicked();

    // 提示信息点击槽函数
    void on_hint_pushButton_clicked();

    // 编辑字段信息按钮点击槽函数
    void on_edit_column_pushButton_clicked();

    // 表格单元格双击事件槽函数
    void on_information_tableWidget_cellDoubleClicked(int row, int column);

    // 表格单元格变化事件槽函数
    void on_information_tableWidget_cellChanged(int row, int column);

    // 单页显示数据量下拉框内容改变事件槽函数
    void on_signle_page_show_num_comboBox_currentTextChanged(const QString &arg1);

    // 上一页点击槽函数
    void on_pre_page_pushButton_clicked();

    // 下一页点击槽函数
    void on_next_page_pushButton_clicked();

    // 页码输入框内容改变事件槽函数
    void on_page_num_lineEdit_textChanged(const QString &arg1);


    //----------------------------------------------- 非界面转到槽的函数，而是在代码中绑定的自定义槽函数----------------------------

    // 条件选择下拉框中选择后显示信息改变槽函数
    void conditionListWidgetTextChanged(QString text);

    // 条件选择下拉框中复选框状态改变事件槽函数
    void conditionListWidgetStatusChanged(int index);

    // 列显示隐藏下拉框中选择后显示信息改变槽函数
    void columnhHideShowListWidgetTextChanged(QString text);

    // 列显示隐藏下拉框中复选框状态改变事件槽函数
    void columnhHideShowListWidgetStatusChanged(int index);

private:
    Ui::CommonForm *ui;
    // 该对象对应的表信息
    UserDefineTableEntity table;

    // 该对象对应的界面表格是否是进行单元格修改设置的信息
    CellInfo informationTableCellUpdateBeforeInfo = CellInfo();


    //---------------------------------------------------条件查询下拉选择框（查询条件组合下拉框）相关-------------------------------
    // 条件查询下拉选择框数据列表
    QListWidget *conditionListWidget = new QListWidget(this);
    // 条件查询下拉选择框显示提示
    QLineEdit *conditionLineEdit = new QLineEdit(this);
    // 是否初始化了条件下拉选择框的设置，这个参数作用是避免重复设置下拉框模式属性等，没有这个判断重复设置就会报错
    bool isInitConditionComboBoxSetting = false;


    //---------------------------------------------------显示隐藏列下拉框相关---------------------------------------------------
    // 显示隐藏列下拉框数据列表
    QListWidget *columnHideShowListWidget = new QListWidget(this);
    // 显示隐藏列下拉框显示提示
    QLineEdit *columnHideShowLineEdit = new QLineEdit(this);
    // 是否初始化了显示隐藏列下拉框的设置，这个参数作用是避免重复设置下拉框模式属性等，没有这个判断重复设置就会报错
    bool isInitColumnHideShowComboBoxSetting = false;


    //--------------------------------------------------界面表格自动调整列宽相关-------------------------------------------------
    // window启动时是否已经调整过表格
    bool isWindowHasStarted = false;
    // 窗口大小改变事件
    virtual void resizeEvent(QResizeEvent * event);
    // 图形绘制事件
    virtual void paintEvent(QPaintEvent *event);
    // 改变isWindowHasStarted（window启动时是否已经调整过表格）
    void changeWindowHasStartedStatus(bool status);


    //---------------------------------------------------下拉框操作通用方法--------------------------------------------------------
    /**
     * @brief getComboboxCheckedSelectColumns 获取指定下拉框已经选中的行数据集合
     * @param widget 下拉框列表widget
     * @return 选中行的数据集合
     */
    QList<QString> getComboboxCheckedSelectColumns(QListWidget* widget);

    /**
     * @brief getAndSetTitleShowForComboboxCheckedSelectColumns 获得指定下拉框选中的行数据集合并设置title显示，返回选中下拉框值集合
     * @param widget 下拉框列表widget
     * @param conditionLineEdit 显示输入
     * @param defaultTitleInfo 默认提示信息，就是没有选中时的提示
     * @return  选中行的数据集合
     */
    QList<QString> getAndSetTitleShowForComboboxCheckedSelectColumns(QListWidget* widget, QLineEdit *conditionLineEdit, QString defaultTitleInfo);

    //---------------------------------------------------其他自定义方法--------------------------------------------------------
    /**
     * @brief userDefineTableWidgetCellChangeUpdateTable 该对象界面对应表格information_tableWidget单元格改变修改数据库表操作
     * @param row 所在行
     * @param column 所在列
     */
    void infomationTableWidgetCellChangeUpdateTable(int row, int column);
};

#endif // COMMONFORM_H
