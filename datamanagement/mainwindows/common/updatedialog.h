#ifndef UPDATEDIALOG_H
#define UPDATEDIALOG_H

#include <QDialog>
#include <QLineEdit>

namespace Ui {
class updateDialog;
}

class updateDialog : public QDialog
{
    Q_OBJECT

public:
    explicit updateDialog(QWidget *parent = 0);
    ~updateDialog();

    void initLayOut(int updateId);

private slots:
    void on_update_pushButton_clicked();

private:
    Ui::updateDialog *ui;
    int updateId = 0;
    QMap<int, QLineEdit*> inputTextMap;
    void userDefinedlayout();
};

#endif // UPDATEDIALOG_H
