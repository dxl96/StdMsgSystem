#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlDriver>
#include <QtWidgets>
#include <QTimer>
#include "datamanagement/columnsetting/columnsettingdialog.h"
#include "datamanagement/userdefine/userdefine.h"
#include "datamanagement/mainwindows/common/commonform.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();

    /**
     * @brief refreshClassifyListWidgetAndTableWidgetByTableName 刷新左侧列表信息到最新，和刷新指定右侧tableName对应表格界面组件CommionForm所有界面信息
     * （该方法主要针对新增、删除、修改表字段后的刷新操作，和新增表后的刷新操作）
     * @param tableName 表名
     */
    void refreshClassifyListWidgetAndTableWidgetByTableName(QString tableName);

    /**
     * @brief refreshClassifyListWidgetAndTableWidgetByTableZnName 刷新左侧列表信息到最新，和刷新指定右侧tableZnName对应表格界面组件CommionForm所有界面信息
     * （该方法主要针对新增、删除、修改表字段后的刷新操作，和新增表后的刷新操作）
     * @param tableZnName 表中文名
     */
    void refreshClassifyListWidgetAndTableWidgetByTableZnName(QString tableZnName);

    /**
     * @brief refreshClassifyListWidgetAndTableWidget 刷新左侧列表信息到最新，和刷新指定右侧tableZnName对应表格界面组件CommionForm所有界面信息
     * （该方法主要针对删除表后的刷新操作）
     */
    void refreshClassifyListWidgetAndTableWidget();

    /**
     * @brief replaceAndRefreshClassifyListWidgetAndTableWidget 刷新左侧列表信息到最新，替换老的表中文名和CommionForm映射为新的表名和新的CommionForm映射
     * （该方法主要针对修改表名后的刷新操作）
     * @param oldTableZnName 老的表中文名
     * @param newTableZnName 新的表中文名
     */
    void replaceAndRefreshClassifyListWidgetAndTableWidget(QString oldTableZnName, QString newTableZnName);

    /**
     * @brief replaceTableWidgetShow 添加tableZnName对应的CommonForm进映射关系,同时根据isReplaceShow设置进行替换右侧表格显示，
     * 指定是否用tableZnName对应的CommonForm替换界面右侧的正在展示的表界面组件CommonForm信息
     * @param tableZnName 表中文名
     * @param isReplaceShow 是否界面进行替换显示
     * @param tableZnName对应的CommonForm界面组件对象
     */
    CommonForm* replaceOrAddTableWidgetShow(QString tableZnName, bool isReplaceShow);

    /**
     * @brief changeProgressBar 改变进度条进度
     * @param time 时间秒（用来做动态加载，会在指定时间缓慢移动到指定值,如果时间小于等于0，则立即设置到达指定值,特殊情况：当计算的执行周期乘以执行次数小于指定时间时，最终移动到指定值的时间要大于指定的time）
     * @param val 指定值
     */
    void changeProgressBar(int time, int val);

private slots:
    //-----------------------------------------------由界面转到槽的函数------------------------------------------------------
    // 分类列表（对应数据库中自定义表数据）中列表项点击事件槽函数
    void on_classify_listWidget_itemClicked(QListWidgetItem *item);

    //-----------------------------------------------非界面转到槽的函数，而是在代码中绑定的自定义槽函数----------------------------
    // 改变背景图片槽函数
    void changeBackGroundImage();

    // 改变进度条的值
    void changeProgressBarVal();
private:
    Ui::MainWindow *ui;

    // 更换背景定时器
    QTimer *changeBackGroundImageime;
    // 装背景图片地址的集合
    QList<QString> backgroundImages;
    // 当前指向backgroundImages集合的索引值
    int nowBackgroundImagesIndex = 0;

    // 进度条变化定时器
    QTimer *progressBarTimer = NULL;
    // 进度条最终值
    int progressBarFanalVal = 0;
    // 进度条当前值
    double progressBarNowVal = 0.00;
    // 进度条指定到达时间（秒）
    int progressTime = 0;
    // 进度条固定执行次数
    int progressFixedCount = 100;


    // 水平分割器
    QSplitter* mainSplitter;
    // 表中文名和自定义表格界面CommonForm的映射关系
    QMap<QString, CommonForm*> tableAndWidgetMap;

    // 向集合backgroundImages中添加图片地址
    void addBackGroundImageime();
    // 打开或创建数据库
    void openDataBase();
    // 创建学生信息表
    bool createStudentSqlTable();
    // 获取系统默认表格列（也就是学生表的字段数据）
    QList<QPair<QString,QString>> getDefaultColumns();
    // 向自定义表中增加默认表名信息数据（也就是学生表的表名信息）
    void insertToUserdefineTableDefaultTableName();
    // 向字段表中增加默认数据（也就是增加学生表对应的字段数据到自定义字段表中）
    void insertToColumnTableDefaultColumns();
    // 初始化分类列表数据（也就是刷新左侧自定义表名列表的信息）,并返回所有自定义表的信息
    QList<UserDefineTableEntity> initClassifyListWidget();
};
#endif // MAINWINDOW_H
