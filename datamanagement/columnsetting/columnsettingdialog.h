#ifndef COLUMNSETTINGDIALOG_H
#define COLUMNSETTINGDIALOG_H

#include <QDialog>
#include "insaertcolumndialog.h"
#include "inserttabledialog.h"
#include <QtWidgets>
#include <QSqlQuery>
#include <QSqlError>
#include "datamanagement/userdefine/userdefine.h"

namespace Ui {
class columnSettingDialog;
}

class columnSettingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit columnSettingDialog(QWidget *parent = 0);
    ~columnSettingDialog();

    void createUserDefineTableHeader();

    void refreshUserDefineTableData();

    void createCloumnTableHeader();

    void refreshCloumnTableData();

    void refreshTableComboBox();

private slots:
    // ------------------------自定义字段表相关的槽函数（界面右侧）-------------------------------------
    void on_add_pushButton_clicked();

    void on_remove_pushButton_clicked();

    void on_columntableWidget_cellDoubleClicked(int row, int column);

    void on_columntableWidget_cellChanged(int row, int column);

    void on_table_comboBox_currentIndexChanged(const QString &arg1);


    // ------------------------自定义表相关的槽函数（界面左侧）-------------------------------------
    void on_add_table_pushButton_clicked();

    void on_remove_table_pushButton_clicked();

    void on_userdefineTable_tableWidget_cellChanged(int row, int column);

    void on_userdefineTable_tableWidget_cellDoubleClicked(int row, int column);

private:
    Ui::columnSettingDialog *ui;

    //-------------------------------------自定义字段表及其对应表格相关（界面右侧）--------------------------------------------------------------------
    // 自定义字段表中文名称和数据库实际字段名映射关系
    QMap<QString, QString> columnTableZnColumnNameAndColumnNames;
    // 自定义字段表格是否是进行单元格修改设置的信息
    CellInfo columnTableCellUpdateBeforeInfo = CellInfo();

    /**
     * @brief initColumnTableZnColumnNameAndColumnNames 初始化自定义字段表中文与数据库实际字段名映射关系
     */
    void initColumnTableZnColumnNameAndColumnNames();

    /**
     * @brief columnTableWidgetCellChangeUpdateTable 自定义字段表格单元格改变修改数据库表操作
     * @param row 所在行
     * @param column 所在列
     */
    void columnTableWidgetCellChangeUpdateTable(int row, int column);

    /**
     * @brief columnTableCellContentIsChange 自定字段表格单元格内容是否改变
     * @param columnZnName 列中文名
     * @param text 修改后的值
     * @param columnEntity 未修改前的该行对应的数据
     * @return 是否改变
     */
    bool columnTableCellContentIsChange(QString columnZnName, QString text, ColumnEntity columnEntity);

    /**
     * @brief getColumnEntityAttributeValue 获取自定义字段表实体对象指定属性的值
     * @param columnZnName 列中文名
     * @param columnEntity 未修改前的该行对应的数据
     * @return 指定字段的值
     */
    QString getColumnEntityAttributeValue(QString columnZnName, ColumnEntity columnEntity);


    //-------------------------------------自定义表及其对应表格相关（界面左侧）--------------------------------------------------------------------
    // 自定义表中文名称和数据库实际字段名映射关系
    QMap<QString, QString> userDefineTableZnColumnNameAndColumnNames;
    // 自定义表格是否是进行单元格修改设置的信息
    CellInfo userDefineTableCellUpdateBeforeInfo = CellInfo();

    /**
     * @brief initUserDefineTableZnColumnNameAndColumnNames 初始化自定义表中文与数据库实际字段名映射关系
     */
    void initUserDefineTableZnColumnNameAndColumnNames();

    /**
     * @brief userDefineTableWidgetCellChangeUpdateTable 自定义表格单元格改变修改数据库表操作
     * @param row 所在行
     * @param column 所在列
     */
    void userDefineTableWidgetCellChangeUpdateTable(int row, int column);

    /**
     * @brief userDefineTableCellContentIsChange 自定字段表格单元格内容是否改变
     * @param columnZnName 列中文名
     * @param text 修改后的值
     * @param userDefineTableEntity 未修改前的该行对应的数据
     * @return 是否改变
     */
    bool userDefineTableCellContentIsChange(QString columnZnName, QString text, UserDefineTableEntity userDefineTableEntity);

    /**
     * @brief getUserDefineEntityAttributeValue 获取自定义字段表实体对象指定属性的值
     * @param columnZnName 列中文名
     * @param userDefineTableEntity 未修改前的该行对应的数据
     * @return 指定字段的值
     */
    QString getUserDefineEntityAttributeValue(QString columnZnName, UserDefineTableEntity userDefineTableEntity);
};

#endif // COLUMNSETTINGDIALOG_H
