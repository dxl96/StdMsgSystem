#ifndef INSAERTCOLUMNDIALOG_H
#define INSAERTCOLUMNDIALOG_H

#include <QDialog>
#include <QString>
#include <QtWidgets>
#include <QSqlQuery>
#include <QSqlDriver>
#include <QSqlError>
#include "datamanagement/userdefine/userdefine.h"

namespace Ui {
class insaertColumnDialog;
}

class insaertColumnDialog : public QDialog
{
    Q_OBJECT

public:
    explicit insaertColumnDialog(QWidget *parent = 0);
    ~insaertColumnDialog();

private slots:
    void on_submit_pushButton_clicked();

private:
    Ui::insaertColumnDialog *ui;
    // 自定义字段表-属性字段中文名和属性字段名映射关系
    QMap<QString, QString> tableZnColumnNameAndColumnNames;
    // 属性字段中文名和组件（输入框）映射关系
    QMap<QString, QObject*> inputTextMap;
    // 界面布局
    void userDefinedlayout();
};

#endif // INSAERTCOLUMNDIALOG_H
