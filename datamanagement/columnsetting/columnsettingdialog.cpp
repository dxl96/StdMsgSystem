#include "columnsettingdialog.h"
#include "ui_columnsettingdialog.h"
#include "datamanagement/mainwindows/mainwindow.h"

columnSettingDialog::columnSettingDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::columnSettingDialog)
{
    ui->setupUi(this);
    // 去掉问号，设置最小化、最大化、关闭
    setWindowFlags(Qt::Dialog | Qt::WindowMinimizeButtonHint | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint);
    setWindowTitle("表、字段编辑");

    // 用分割器替换桌面的QWidget，因为界面没有分割器，只有用这种方式进行替换，如果是纯代码写的界面，不存在这种问题
    QSplitter * mainSplitter = new QSplitter(Qt::Horizontal);//新建水平分割器
    mainSplitter->setHandleWidth(3);//分割线的宽度
    mainSplitter->setChildrenCollapsible(false);//不允许把分割出的子窗口拖小到0，最小值被限定为sizeHint或maxSize/minSize
    mainSplitter->addWidget(ui->add_table_widget);//把ui中拖出的各个控件拿走，放到分割器里面
    mainSplitter->addWidget(ui->add_column_widget);
    this->layout()->replaceWidget(ui->all_widget, mainSplitter);//把ui中拖出的容器QWidget换成分割器

    // 初始化自定义表表格列头与数据库字段的对应关系
    initUserDefineTableZnColumnNameAndColumnNames();
    // 创建自定义用户表表格列头
    createUserDefineTableHeader();
    // 刷新自定义用户表表格数据
    refreshUserDefineTableData();

    // 初始化自定义字段表表格列头与数据库字段的对应关系
    initColumnTableZnColumnNameAndColumnNames();
    // 创建自定义字段表表格列头
    createCloumnTableHeader();
    // 刷新自定义字段表表格数据
    refreshCloumnTableData();
    // 初始化自定义表名称信息下拉框数据
    refreshTableComboBox();
}

columnSettingDialog::~columnSettingDialog()
{
    delete ui;
}

// ==============初始化自定义字段表中中文名和字段名的映射关系===============================================================
void columnSettingDialog::initColumnTableZnColumnNameAndColumnNames()
{
    columnTableZnColumnNameAndColumnNames.insert(UserDefineConstant::TABLE_CONNECT_TABLE_ZN_TITLE,UserDefineConstant::TABLE_CONNECT_TABLE);
    columnTableZnColumnNameAndColumnNames.insert(UserDefineConstant::TABLE_COLUMN_NAME_ZN_TITLE,UserDefineConstant::TABLE_COLUMN_NAME);
    columnTableZnColumnNameAndColumnNames.insert(UserDefineConstant::TABLE_COLUMN_ZN_NAME_ZN_TITLE,UserDefineConstant::TABLE_COLUMN_ZN_NAME);
    columnTableZnColumnNameAndColumnNames.insert(UserDefineConstant::TABLE_COLUMN_IS_UNIQUE_ZN_TITLE,UserDefineConstant::TABLE_COLUMN_IS_UNIQUE);
    columnTableZnColumnNameAndColumnNames.insert(UserDefineConstant::TABLE_COLUMN_IS_REQUIRED_ZN_TITLE,UserDefineConstant::TABLE_COLUMN_IS_REQUIRED);
}

// ==============创建自定义表对应的界面表格的表头显示===============================================================
void columnSettingDialog::createUserDefineTableHeader()
{
    QList<QString> znColumns;
    znColumns.append(UserDefineConstant::CHECK_BOX_CN_NAME);
    znColumns.append(UserDefineConstant::ID_ZN_NAME);
    znColumns.append(UserDefineConstant::USERDEFINE_TABLE_COLUMN_TABLE_NAME_ZN_TITLE);
    znColumns.append(UserDefineConstant::USERDEFINE_TABLE_COLUMN_TABLE_ZN_NAME_ZN_TITLE);
    TableWidgetOpt::createQTableWidgetHeader(ui->userdefineTable_tableWidget, znColumns);
}

// ==============刷新自定义表对应的界面表格的数据信息===============================================================
void columnSettingDialog::refreshUserDefineTableData()
{
    QList<QString> columns;
    columns.append(UserDefineConstant::ID_COLUMN_NUIQUE_NAME);
    columns.append(UserDefineConstant::USERDEFINE_TABLE_COLUMN_TABLE_NAME);
    columns.append(UserDefineConstant::USERDEFINE_TABLE_COLUMN_TABLE_ZN_NAME);
    TableWidgetOpt::selectDataBaseTableDataToQTableWidget(ui->userdefineTable_tableWidget, UserDefineConstant::USERDEFINE_TABLE_TABLE_NAME, columns, 1, 10, QList<QueryConditions>(), false);
}

// ==============创建自定义字段表对应的界面表格的表头显示===============================================================
void columnSettingDialog::createCloumnTableHeader()
{
    QList<QString> znColumns;
    znColumns.append(UserDefineConstant::CHECK_BOX_CN_NAME);
    znColumns.append(UserDefineConstant::ID_ZN_NAME);
    znColumns.append(UserDefineConstant::TABLE_CONNECT_TABLE_ZN_TITLE);
    znColumns.append(UserDefineConstant::TABLE_COLUMN_NAME_ZN_TITLE);
    znColumns.append(UserDefineConstant::TABLE_COLUMN_ZN_NAME_ZN_TITLE);
    znColumns.append(UserDefineConstant::TABLE_COLUMN_IS_UNIQUE_ZN_TITLE);
    znColumns.append(UserDefineConstant::TABLE_COLUMN_IS_REQUIRED_ZN_TITLE);
    TableWidgetOpt::createQTableWidgetHeader(ui->columntableWidget, znColumns);
}

// ==============刷新自定义字段表对应的界面表格的数据信息===============================================================
void columnSettingDialog::refreshCloumnTableData()
{
    // 构造自定义字段表查询字段信息
    QList<QString> columns;
    columns.append(UserDefineConstant::ID_COLUMN_NUIQUE_NAME);
    columns.append(UserDefineConstant::TABLE_CONNECT_TABLE);
    columns.append(UserDefineConstant::TABLE_COLUMN_NAME);
    columns.append(UserDefineConstant::TABLE_COLUMN_ZN_NAME);
    columns.append(UserDefineConstant::TABLE_COLUMN_IS_UNIQUE);
    columns.append(UserDefineConstant::TABLE_COLUMN_IS_REQUIRED);
    // 获取界面表中文名下拉框当前选中行数据
    QString tableZnName = ui->table_comboBox->currentText().trimmed();
    QString tableName = "";
    // 如当前选中表中文名为空的话，获取自定义表中所有表名的第一个作为查询条件
    if(ObjectUtil::isEmpty(tableZnName)) {
        // 查询自定义表所有数据
        QList<UserDefineTableEntity> userDefineTableEntitys = DataBaseOpt::selectAllUserDefineTable(QList<QueryConditions>());
        if(!ObjectUtil::isEmpty(userDefineTableEntitys)) {
            UserDefineTableEntity entity = userDefineTableEntitys.at(0);// 以第一条数据作为条件
            tableName = entity.tableName;
        }
    } else { // 如果下拉框选中的表中文名内容不为空，则从数据库查询出相应的表名
        QList<QueryConditions> conditions;
        conditions.append(QueryConditions(UserDefineConstant::USERDEFINE_TABLE_COLUMN_TABLE_ZN_NAME, tableZnName, false, "=", "and"));
        QList<UserDefineTableEntity> userDefineTableEntitys = DataBaseOpt::selectAllUserDefineTable(conditions);
        UserDefineTableEntity entity = userDefineTableEntitys.at(0); // 因为有表中文名条件限制，所以查询出只可能是一条数据，所以我们取第一条即可
        tableName = entity.tableName;
    }
    // 构造自定义字段表查询条件
    QList<QueryConditions> conditions;
    if(!ObjectUtil::isEmpty(tableName)) {
        conditions.append(QueryConditions(UserDefineConstant::TABLE_CONNECT_TABLE, tableName, false, "=", "and"));
    }
    // 刷新自定义字段表格数据显示
    TableWidgetOpt::selectDataBaseTableDataToQTableWidget(ui->columntableWidget, UserDefineConstant::COLUMN_TABLE_NAME, columns, 1, 10, conditions, false);
}

// ==============刷新表名下拉框的数据展示===============================================================
void columnSettingDialog::refreshTableComboBox()
{
    // 清空表名下拉列表数据
    ui->table_comboBox->clear();
    // 查找自定义表中记录的表信息，组装中文名称集合
    QList<UserDefineTableEntity> userdefineTableEntitys = DataBaseOpt::selectAllUserDefineTable(QList<QueryConditions>());
    QList<QString> tableNames;
    for(UserDefineTableEntity entity: userdefineTableEntitys) {
        tableNames.append(entity.tableZnName);
    }
    // 添加到下拉列表中展示
    QStringList tableNamesList = QStringList(tableNames);
    ui->table_comboBox->addItems(tableNamesList);
}

// ==============自定义字段表（自定义字段表格）信息【添加】按钮点击事件===============================================================
void columnSettingDialog::on_add_pushButton_clicked()
{
    insaertColumnDialog* insaertColumn = new insaertColumnDialog(this);
    insaertColumn->setAttribute(Qt::WA_DeleteOnClose);
    insaertColumn->setModal(true);
    insaertColumn->show();
}

// ==============自定义字段表（自定义字段表格）信息【删除】按钮点击事件===============================================================
void columnSettingDialog::on_remove_pushButton_clicked()
{
    // 获得选中行的id集合
    QList<int> ids = TableWidgetOpt::getQTableWidgetCheckedIds(ui->columntableWidget);
    qDebug()<<"选中大小:"<<ids.size();
    if(ObjectUtil::isEmpty(ids)) {
        QMessageBox::information(this,"提示","请勾选要删除的行数据");
        return;
    }
    int sign = QMessageBox::information(this, "提示", "删除后不可恢复，确认删除", "确认","取消", 0, 1);
    if(sign == 1) {
        return;
    }
    // 开启一个加载提示框
    LoadingDialog* loading = LoadingDialog::createLoading(this);

    // 查询出数据库中自定义字段表相应id对应的数据
    QList<ColumnEntity> columnEntitys = DataBaseOpt::selectColumnTableByIds(ids);

    if(DataBaseManage::getDb().driver()->hasFeature(QSqlDriver::Transactions)) { // 判断是否支持事务
        if(DataBaseManage::getDb().transaction()) { // 判断是否开启事务成功
            QList<bool> isSucess;
            QList<QString> tableNames;
            for(ColumnEntity entity: columnEntitys) {
                // 删除自定义字段表对应行数据
                QList<int> deleteId;
                deleteId.append(entity.id);
                isSucess.append(DataBaseOpt::removeTableByIds(UserDefineConstant::COLUMN_TABLE_NAME, deleteId));
                // 删除自定义表中关联表（connectTable）对应的字段
                QList<QString> columnNames;
                columnNames.append(entity.columnName);
                isSucess.append(DataBaseOpt::deleteColumnForTable(entity.connectTable,columnNames));
                if(!tableNames.contains(entity.connectTable)) {
                    tableNames.append(entity.connectTable);
                }
            }
            // 没有错误则提交，有错误则回滚事务
            if(isSucess.contains(false)) {
                if(!DataBaseManage::getDb().rollback()) {  //回滚
                    qDebug()<< DataBaseManage::getDb().lastError();
                }
            } else {
                if(!DataBaseManage::getDb().commit()) {  //提交
                    qDebug()<< DataBaseManage::getDb().lastError();
                    if(!DataBaseManage::getDb().rollback()) {  //回滚
                        qDebug()<< DataBaseManage::getDb().lastError();
                    }
                }
            }
            loading->close(); // 关闭加载提示框
            // 成功刷新数据
            if(!isSucess.contains(false)) {
                // 刷新列表
                refreshCloumnTableData();

                // 查询出删除的行对应的关联表（connectTable）列记录的表信息集合，后续刷新这些表对应的CommonForm列表界面数据
                QString tabelNamesStr = "";
                int i = 0;
                for(QString tabelName: tableNames) {
                    if(i == tableNames.size() -1) {
                        tabelNamesStr = tabelNamesStr + "'" + tabelName + "'";
                    } else {
                        tabelNamesStr = tabelNamesStr + "'" + tabelName + "'" + ",";
                    }
                    i++;
                }
                QList<QueryConditions> conditions;
                conditions.append(QueryConditions(UserDefineConstant::USERDEFINE_TABLE_COLUMN_TABLE_NAME+" in ("+ tabelNamesStr+")"));
                QList<UserDefineTableEntity> tableEntitys = DataBaseOpt::selectAllUserDefineTable(conditions);
                // 循环刷新主界面左侧列表信息，和刷新主界面指定右侧tableZnName表格界面所有界面信息
                for(UserDefineTableEntity entity: tableEntitys) {
                    ((MainWindow* )UserDefineConstant::MAIN_WINDOW)->refreshClassifyListWidgetAndTableWidgetByTableZnName(entity.tableZnName);
                }
                QMessageBox::information(this,"成功","删除成功");
            } else {
                QMessageBox::information(this,"失败","删除失败");
            }
        } else {
            loading->close(); // 关闭加载提示框
            QMessageBox::information(this, "失败","开启数据库事务失败，操作失败");
        }
    } else {
        loading->close(); // 关闭加载提示框
        QMessageBox::information(this, "失败","不支持数据库事务，操作失败");
    }
}

// ==============自定义字段表格单元格【双击】事件===============================================================
void columnSettingDialog::on_columntableWidget_cellDoubleClicked(int row, int column)
{
    qDebug()<<"进入修改自定义字段表数据双击单元格操作";
    // 修改该标志的值，告诉后续的cellChanged事件，可以进行修改操作
    columnTableCellUpdateBeforeInfo.isDoubleClick = true;
    columnTableCellUpdateBeforeInfo.row = row;
    columnTableCellUpdateBeforeInfo.column = column;
    columnTableCellUpdateBeforeInfo.text = ui->columntableWidget->item(row, column)->text();
    qDebug()<<"修改自定义字段表数据双击单元格操作,赋值："
           << "isDoubleClick = " << columnTableCellUpdateBeforeInfo.isDoubleClick
           << ",row = " << columnTableCellUpdateBeforeInfo.row
           << ",column = " << columnTableCellUpdateBeforeInfo.column
           << ",text = " << columnTableCellUpdateBeforeInfo.text;
}

// ==============自定义字段表格【单元格改变】事件===============================================================
void columnSettingDialog::on_columntableWidget_cellChanged(int row, int column)
{
    qDebug()<<"修改自定义字段表数据操作,读columnTableCellUpdateBeforeInfo值："
           << "isDoubleClick = " << columnTableCellUpdateBeforeInfo.isDoubleClick
           << ",row = " << columnTableCellUpdateBeforeInfo.row
           << ",column = " << columnTableCellUpdateBeforeInfo.column
           << ",text = " << columnTableCellUpdateBeforeInfo.text;
    // 判断是否是界面进行了编辑操作，因为这个槽函数在表格不管是内容改变，行数量改变，代码操作单元格等都会触发
    // 所以不管是表格初始化，还是任何单元格有变化就会触发这个槽函数，而我们在单元格变化时不一定要进行修改操作
    // 这里需要阻断，我们只需要界面通过双击单元格后进行的操作才认为是修改操作
    // 所以判断是否进行双击单元格，判断双击的单元格行列是否等于现在改变单元格的行列，判断双击时老的单元格内容是否不等于改变时单元格的内容
    // 任何一个不满足，都直接进行返回，这么判断原因是首先要确认是双击时进行的判断，行列值保证双击时和修改时是同一个单元格，内容判断是
    // 保证用户可能双击单元格后并没有修改内容，这样该事件就不会触发，同时用户再可能将进行一些刷新列表什么的操作，这样该事件就会被反复触发
    // 而因为之前点击了单元格，所以是否双击单元格的bool值是否为true，和行列相等的条件在刷新到相同单元格内容时一定是满足条件的，通过内容是否不同，
    // 不同才进行修改，就可以阻断这种操作到一半不操作的情况，从而保证修改流程不出错
    if(!(columnTableCellUpdateBeforeInfo.isDoubleClick
         && row == columnTableCellUpdateBeforeInfo.row
         && column == columnTableCellUpdateBeforeInfo.column
         && columnTableCellUpdateBeforeInfo.text != ui->columntableWidget->item(row, column)->text())) {
        return;
    }
    // 进入阻塞模式，上面说到，很多情况会触发这个函数，如果不阻塞，处理方法中含有循环，判断等情况，因为数据相互干扰，可能发生程序死锁，导致崩溃
    // 这里阻塞后，那么表格所有的信号都会被阻塞，在本次处理结束前，就不会有其他情况进行干扰，避免死锁
    ui->columntableWidget->blockSignals(true);

    // 修改处理
    columnTableWidgetCellChangeUpdateTable(row, column);

    // 处理完成后解除阻塞模式
    ui->columntableWidget->blockSignals(false);

    // 修改操作完之后恢复原始的是否进行修改操作的标志信息，保证下一次修改流程的从头开始
    columnTableCellUpdateBeforeInfo.isDoubleClick = false;
    columnTableCellUpdateBeforeInfo.row = 0;
    columnTableCellUpdateBeforeInfo.column = 0;
    columnTableCellUpdateBeforeInfo.text = QString();
}

// ==============自定义字段表格【单元格改变】事件调用的修改内容方法===============================================================
void columnSettingDialog::columnTableWidgetCellChangeUpdateTable(int row, int column)
{
    // 开启一个加载提示框
    LoadingDialog* loading = LoadingDialog::createLoading(this);

    qDebug()<<"进入修改自定义字段表数据操作";
    // 获取表格中修改的值
    QTableWidgetItem* item = ui->columntableWidget->item(row, column);
    QString text = item->text().trimmed();
    // 获取修改单元格对应的列头(中文名列头)
    QTableWidgetItem* horizontalHeaderItem = ui->columntableWidget->horizontalHeaderItem(column);
    QString columnZnName = horizontalHeaderItem->text(); // 列头中文名
    // 获得该列头在自定义字段表中的字段名称
    QString columnName = columnTableZnColumnNameAndColumnNames.value(columnZnName);
    // 从数据库中查询出该行对应的老数据
    int idColumnNum = TableWidgetOpt::findQTableWidgetIdZnNameColumn(ui->columntableWidget); // 获得修改行的id列所在索引值
    int nowRowId = ui->columntableWidget->item(row, idColumnNum)->text().toInt(); // 获得id列在该行的值
    QList<int> ids;
    ids.append(nowRowId); // 构造查询参数
    QList<ColumnEntity> oldRowDatas = DataBaseOpt::selectColumnTableByIds(ids); // 查询出数据库对应数据
    ColumnEntity oldColumnEntity = oldRowDatas.at(0); // 老数据（因为实际只能一次操作一行，根据id查询，id也只有一个，所以只有一个结果，直接获取即可）
    if(ObjectUtil::isEmpty(text)) {
        loading->close(); // 关闭加载提示框
        QMessageBox::information(this, "提示","填写内容不能为空");
        item->setText(getColumnEntityAttributeValue(columnZnName, oldColumnEntity)); // 恢复单元格未修改前的值
        return;
    }
    // 内容没有变化直接返回
    if(!columnTableCellContentIsChange(columnZnName, text, oldColumnEntity)) {
        loading->close(); // 关闭加载提示框
        item->setText(getColumnEntityAttributeValue(columnZnName, oldColumnEntity)); // 恢复单元格未修改前的值
        return;
    }
    // 进行页面单元格修改填写的信息正确性进行判断
    // 是否必填列和是否唯一列，如果填的不是“是/否”，则直接返回提示错误信息
    if(columnZnName == UserDefineConstant::TABLE_COLUMN_IS_UNIQUE_ZN_TITLE || columnZnName == UserDefineConstant::TABLE_COLUMN_IS_REQUIRED_ZN_TITLE) {
        qDebug()<<"填写数据是否为true："<<columnZnName<<","<<(text == UserDefineConstant::BOOL_ZN_NAME_TRUE);
        if(!(text == UserDefineConstant::BOOL_ZN_NAME_TRUE) && !(text == UserDefineConstant::BOOL_ZN_NAME_FALSE)) {
            loading->close(); // 关闭加载提示框
            QMessageBox::information(this, "提示",columnZnName+"请填写【"+UserDefineConstant::BOOL_ZN_NAME_TRUE+","+UserDefineConstant::BOOL_ZN_NAME_FALSE+"】");
            item->setText(getColumnEntityAttributeValue(columnZnName, oldColumnEntity)); // 恢复单元格未修改前的值
            return;
        }
    }
    // 字段名称列和字段中文名称列内容校验
    if(columnZnName == UserDefineConstant::TABLE_COLUMN_NAME_ZN_TITLE || columnZnName == UserDefineConstant::TABLE_COLUMN_ZN_NAME_ZN_TITLE) {
        QList<QueryConditions> conditions;
        conditions.append(QueryConditions(UserDefineConstant::TABLE_CONNECT_TABLE, oldColumnEntity.connectTable, false, "=", "and"));
        conditions.append(QueryConditions(columnName, text, false, "=", "and"));
        // 字段名称列如果不是英文字母和下划线的组合词直接返回提示错误信息
        if(columnZnName == UserDefineConstant::TABLE_COLUMN_NAME_ZN_TITLE) {
            QRegExp rx("^[a-zA-Z_]+$");
            if(rx.indexIn(text) == -1){
                loading->close(); // 关闭加载提示框
                QMessageBox::information(this, "提示",columnZnName+"请填写英文字母和下划线的组合词");
                item->setText(getColumnEntityAttributeValue(columnZnName, oldColumnEntity)); // 恢复单元格未修改前的值
                return;
            }
            // 添加不等于自身老数据条件(排除自身原数据是因为修改可以为自身原来的数据，新数据才检查是否存在)
            conditions.append(QueryConditions(columnName, oldColumnEntity.columnName, false, "!=", "and"));
        } else {
            // 添加不等于自身老数据条件(排除自身原数据是因为修改可以为自身原来的数据，新数据才检查是否存在)
            conditions.append(QueryConditions(columnName, oldColumnEntity.columnZnName, false, "!=", "and"));
        }
        // 数据库中如果已经存在了填写的字段名称列或字段中文名称列的内容，则直接返回提示错误信息
        int count = DataBaseOpt::selectAllCount(UserDefineConstant::COLUMN_TABLE_NAME, conditions);
        if(count > 0) {
            loading->close(); // 关闭加载提示框
            QMessageBox::information(this, "提示",columnZnName+"已存在，请重新输入");
            item->setText(getColumnEntityAttributeValue(columnZnName, oldColumnEntity)); // 恢复单元格未修改前的值
            return;
        }
    }
    QString newTableName = "";
    // 如果修改的是关联表字段
    if(columnZnName == UserDefineConstant::TABLE_CONNECT_TABLE_ZN_TITLE) {
        QRegExp rx("^[a-zA-Z_]+$");
        if(rx.indexIn(text) == -1){ // 表名必须是英文字母和下滑线的组合
            loading->close(); // 关闭加载提示框
            QMessageBox::information(this, "提示",columnZnName+"请填写英文字母和下划线的组合词");
            item->setText(getColumnEntityAttributeValue(columnZnName, oldColumnEntity)); // 恢复单元格未修改前的值
            return;
        }
        // 查询填写的表名存不存在，不存在返回错误提示信息
        QList<QueryConditions> conditions;
        conditions.append(QueryConditions(UserDefineConstant::USERDEFINE_TABLE_COLUMN_TABLE_NAME, text, false, "=", "and"));
        int count = DataBaseOpt::selectAllCount(UserDefineConstant::USERDEFINE_TABLE_TABLE_NAME, conditions);
        if(count < 1) {
            loading->close(); // 关闭加载提示框
            QMessageBox::information(this, "提示","不存在表"+ text+",请仔细检查,修改为已经添加的表名");
            item->setText(getColumnEntityAttributeValue(columnZnName, oldColumnEntity)); // 恢复单元格未修改前的值
            return;
        }
        // 查询修改过后的关联表中包不包含改行的字段名列的数据（字段名列的内容所对应的字段名），如果关联表已经存在了该字段，则返回错误提示信息
        QList<QueryConditions> columnConditions;
        columnConditions.append(QueryConditions(UserDefineConstant::TABLE_CONNECT_TABLE, text, false, "=", "and"));
        columnConditions.append(QueryConditions(UserDefineConstant::TABLE_COLUMN_NAME, oldColumnEntity.columnName, false, "=", "or"));
        columnConditions.append(QueryConditions(UserDefineConstant::TABLE_COLUMN_ZN_NAME, oldColumnEntity.columnZnName, false, "=", "and"));
        QList<ColumnEntity> columnResult = DataBaseOpt::selectAllColumnTable(columnConditions);
        if(columnResult.size() < 1) {
            loading->close(); // 关闭加载提示框
            QMessageBox::information(this, "提示","修改关联表"+ text+"中已经包含字段"+oldColumnEntity.columnName+",或者"+oldColumnEntity.columnZnName+",请仔细检查");
            item->setText(getColumnEntityAttributeValue(columnZnName, oldColumnEntity)); // 恢复单元格未修改前的值
            return;
        }
        newTableName = text; // 记录下新修改的关联表
    }
    if(DataBaseManage::getDb().driver()->hasFeature(QSqlDriver::Transactions)) { // 判断是否支持事务
        if(DataBaseManage::getDb().transaction()) { // 判断是否开启事务成功
            QList<bool> isSucess;
            // 更新数据库自定义字段表数据
            QMap<QString, QString> columnNameAndValues;
            columnNameAndValues.insert(columnName, text);
            isSucess.append(DataBaseOpt::updateTableById(UserDefineConstant::COLUMN_TABLE_NAME, oldColumnEntity.id, columnNameAndValues));
            // 如果修改的是字段名列数据，则更新自定义字段表对应表的相应字段名
            if(columnZnName == UserDefineConstant::TABLE_COLUMN_NAME_ZN_TITLE) {
                QMap<QString, QString> oldColumnNameAndNewColumnName;
                oldColumnNameAndNewColumnName.insert(oldColumnEntity.columnName, text);
                isSucess.append(DataBaseOpt::updateColumnForTable(oldColumnEntity.connectTable, oldColumnNameAndNewColumnName));
            }
            // 如果修改的是关联表字段数据data1，得到该行的字段名列数据data2，将该行老数据关联的表的data2字段删除，向data1表添加字段data2
            if(columnZnName == UserDefineConstant::TABLE_CONNECT_TABLE_ZN_TITLE) {
                QList<QString> removeAndAddColumns;
                removeAndAddColumns.append(oldColumnEntity.columnName);
                // 删除老关联表对应的该行字段名
                isSucess.append(DataBaseOpt::deleteColumnForTable(oldColumnEntity.connectTable, removeAndAddColumns));
                // 向新关联表添加该行对应的字段名
                isSucess.append(DataBaseOpt::addColumnForTable(text, removeAndAddColumns));
            }
            // 没有出错则提交，出错则回滚
            if(isSucess.contains(false)) {
                item->setText(getColumnEntityAttributeValue(columnZnName, oldColumnEntity)); // 恢复单元格未修改前的值
                if(!DataBaseManage::getDb().rollback()) {  //回滚
                    qDebug()<< DataBaseManage::getDb().lastError();
                }
            } else {
                if(!DataBaseManage::getDb().commit()) {  //提交
                    qDebug()<< DataBaseManage::getDb().lastError();
                    if(!DataBaseManage::getDb().rollback()) {  //回滚
                        qDebug()<< DataBaseManage::getDb().lastError();
                    }
                }
            }
            loading->close(); // 关闭加载提示框
            // 成功刷新数据
            if(!isSucess.contains(false)) {
                // 刷新列表
                refreshCloumnTableData();

                // 刷新主界面左侧列表信息，和刷新主界面指定右侧tableName表格界面所有界面信息
                ((MainWindow* )UserDefineConstant::MAIN_WINDOW)->refreshClassifyListWidgetAndTableWidgetByTableName(oldColumnEntity.connectTable);
                if(!ObjectUtil::isEmpty(newTableName)) { // 如果修改关联表列数据，则也要刷修改后表名对应的界面数据
                    ((MainWindow* )UserDefineConstant::MAIN_WINDOW)->refreshClassifyListWidgetAndTableWidgetByTableName(newTableName);
                }

                QMessageBox::information(this, "成功","修改成功");
            } else {
                QMessageBox::information(this, "失败","修改失败");
                item->setText(getColumnEntityAttributeValue(columnZnName, oldColumnEntity)); // 恢复单元格未修改前的值
            }
        } else {
            loading->close(); // 关闭加载提示框
            QMessageBox::information(this, "失败","开启数据库事务失败，操作失败");
            item->setText(getColumnEntityAttributeValue(columnZnName, oldColumnEntity)); // 恢复单元格未修改前的值
        }
    } else {
        loading->close(); // 关闭加载提示框
        QMessageBox::information(this, "失败","不支持数据库事务，操作失败");
        item->setText(getColumnEntityAttributeValue(columnZnName, oldColumnEntity)); // 恢复单元格未修改前的值
    }
}

// ==============自定义字段表格【单元格改变】事件调用的判断改变单元格中内容和原来的内容比较是否发生改变方法===============================================================
bool columnSettingDialog::columnTableCellContentIsChange(QString columnZnName,QString text, ColumnEntity columnEntity)
{
    // 内容没变化直接返回
    if(columnZnName == UserDefineConstant::TABLE_COLUMN_IS_UNIQUE_ZN_TITLE && text == columnEntity.columnIsUnique) {
        return false;
    }
    if(columnZnName == UserDefineConstant::TABLE_COLUMN_IS_REQUIRED_ZN_TITLE && text == columnEntity.columnIsRequired) {
        return false;
    }
    if(columnZnName == UserDefineConstant::TABLE_COLUMN_NAME_ZN_TITLE && text == columnEntity.columnName) {
        return false;
    }
    if(columnZnName == UserDefineConstant::TABLE_COLUMN_ZN_NAME_ZN_TITLE && text == columnEntity.columnZnName) {
        return false;
    }
    if(columnZnName == UserDefineConstant::TABLE_CONNECT_TABLE_ZN_TITLE && text == columnEntity.connectTable) {
        return false;
    }
    return true;
}

// ==============自定义字段表格【单元格改变】事件调用的获取指定列修改之前的值===============================================================
QString columnSettingDialog::getColumnEntityAttributeValue(QString columnZnName, ColumnEntity columnEntity)
{
    // 根据列表头获取相对应的数据
    if(columnZnName == UserDefineConstant::TABLE_COLUMN_IS_UNIQUE_ZN_TITLE) {
        return columnEntity.columnIsUnique;
    }
    if(columnZnName == UserDefineConstant::TABLE_COLUMN_IS_REQUIRED_ZN_TITLE) {
        return columnEntity.columnIsRequired;
    }
    if(columnZnName == UserDefineConstant::TABLE_COLUMN_NAME_ZN_TITLE) {
        return columnEntity.columnName;
    }
    if(columnZnName == UserDefineConstant::TABLE_COLUMN_ZN_NAME_ZN_TITLE) {
        return columnEntity.columnZnName;
    }
    if(columnZnName == UserDefineConstant::TABLE_CONNECT_TABLE_ZN_TITLE) {
        return columnEntity.connectTable;
    }
    return QString();
}

// ==============表名下拉框【索引改变】事件===============================================================
void columnSettingDialog::on_table_comboBox_currentIndexChanged(const QString &arg1)
{
    // 刷新自定义字段表格数据
    refreshCloumnTableData();
}


// ==============初始化自定义表中属性字段中文标题和实际数据库字段映射关系===============================================================
void columnSettingDialog::initUserDefineTableZnColumnNameAndColumnNames()
{
    userDefineTableZnColumnNameAndColumnNames.insert(UserDefineConstant::USERDEFINE_TABLE_COLUMN_TABLE_NAME_ZN_TITLE,
                                                     UserDefineConstant::USERDEFINE_TABLE_COLUMN_TABLE_NAME);
    userDefineTableZnColumnNameAndColumnNames.insert(UserDefineConstant::USERDEFINE_TABLE_COLUMN_TABLE_ZN_NAME_ZN_TITLE,
                                                     UserDefineConstant::USERDEFINE_TABLE_COLUMN_TABLE_ZN_NAME);
}

// ==============自定义表对应表格【添加按钮】点击事件
void columnSettingDialog::on_add_table_pushButton_clicked()
{
    insertTableDialog* tableDialog = new insertTableDialog(this);
    tableDialog->setAttribute(Qt::WA_DeleteOnClose);
    tableDialog->setModal(true);
    tableDialog->show();
}

// ==============自定义表对应表格【删除按钮】点击事件
void columnSettingDialog::on_remove_table_pushButton_clicked()
{
    QList<int> ids = TableWidgetOpt::getQTableWidgetCheckedIds(ui->userdefineTable_tableWidget);
    qDebug()<<"选中大小:"<<ids.size();
    if(ids.size() == 0) {
        QMessageBox::information(this,"提示","请勾选要删除的行数据");
        return;
    }
    int sign = QMessageBox::information(this, "提示", "该操作将同步删除该表对应的数据，删除后不可恢复，确认删除", "确认","取消", 0, 1);
    if(sign == 1) {
        return;
    }
    // 开启一个加载提示框
    LoadingDialog* loading = LoadingDialog::createLoading(this);

    QList<UserDefineTableEntity> userDefineTableEntitys = DataBaseOpt::selectUserDefineTableByIds(ids); // 查找出需要删除的对应的自定义表数据
    if(DataBaseManage::getDb().driver()->hasFeature(QSqlDriver::Transactions)) { // 判断是否支持事务
        if(DataBaseManage::getDb().transaction()) { // 判断是否开启事务成功
            QList<bool> isSucess;
            QList<QString> tableNames;
            isSucess.append(DataBaseOpt::removeTableByIds(UserDefineConstant::USERDEFINE_TABLE_TABLE_NAME,ids)); // 删除自定义表中的数据
            // 拼接in语句，删除自定义字段表中关联表为需要删除的表名的数据
            QString inSql = UserDefineConstant::TABLE_CONNECT_TABLE + " in (";
            int i = 0;
            for(UserDefineTableEntity entity: userDefineTableEntitys) {
                tableNames.append(entity.tableName);
                if(i == userDefineTableEntitys.length() -1) {
                    inSql = inSql + "'" + entity.tableName + "'";
                } else {
                    inSql = inSql + "'" + entity.tableName + "'" + ",";
                }
                i++;
            }
            inSql = inSql + ")";
            QList<QueryConditions> conditions;
            conditions.append(QueryConditions(inSql));
            isSucess.append(DataBaseOpt::deleteTableDataByCondition(UserDefineConstant::COLUMN_TABLE_NAME, conditions)); // 删除自定义字段表中的相关数据
            // 删除真正的物理表
            for(QString tableName : tableNames) {
                isSucess.append(DataBaseOpt::removeTable(tableName));
            }
            // 没有错误则提交，有错误则回滚事务
            if(isSucess.contains(false)) {
                if(!DataBaseManage::getDb().rollback()) {  //回滚
                    qDebug()<< DataBaseManage::getDb().lastError();
                }
            } else {
                if(!DataBaseManage::getDb().commit()) {  //提交
                    qDebug()<< DataBaseManage::getDb().lastError();
                    if(!DataBaseManage::getDb().rollback()) {  //回滚
                        qDebug()<< DataBaseManage::getDb().lastError();
                    }
                }
            }
            loading->close(); // 关闭加载提示框
            // 成功刷新数据
            if(!isSucess.contains(false)) {
                // 刷新自定义表列表
                refreshUserDefineTableData();
                // 刷新表中文名下拉框数据
                refreshTableComboBox();
                // 刷新自定义字段列表
                refreshCloumnTableData();

                // 刷新主界面左侧列表信息，和刷新主界面对应右侧表格界面所有界面信息
                ((MainWindow* )UserDefineConstant::MAIN_WINDOW)->refreshClassifyListWidgetAndTableWidget();

                QMessageBox::information(this,"成功","删除成功");
            } else {
                QMessageBox::information(this,"失败","删除失败");
            }
        } else {
            loading->close(); // 关闭加载提示框
            QMessageBox::information(this, "失败","数据库事务开启失败，操作失败");
        }
    } else {
        loading->close(); // 关闭加载提示框
        QMessageBox::information(this, "失败","不支持数据库事务，操作失败");
    }

}

// ==============自定义表对应表格【单元格双击】事件
void columnSettingDialog::on_userdefineTable_tableWidget_cellDoubleClicked(int row, int column)
{
    qDebug()<<"进入修改自定义表数据双击单元格操作";
    // 修改该标志的值，告诉后续的cellChanged事件，可以进行修改操作
    userDefineTableCellUpdateBeforeInfo.isDoubleClick = true;
    userDefineTableCellUpdateBeforeInfo.row = row;
    userDefineTableCellUpdateBeforeInfo.column = column;
    userDefineTableCellUpdateBeforeInfo.text = ui->userdefineTable_tableWidget->item(row, column)->text();
    qDebug()<<"修改自定义表数据双击单元格操作,赋值："
           << "isDoubleClick = " << userDefineTableCellUpdateBeforeInfo.isDoubleClick
           << ",row = " << userDefineTableCellUpdateBeforeInfo.row
           << ",column = " << userDefineTableCellUpdateBeforeInfo.column
           << ",text = " << userDefineTableCellUpdateBeforeInfo.text;
}

// ==============自定义表对应表格【单元格改变】事件
void columnSettingDialog::on_userdefineTable_tableWidget_cellChanged(int row, int column)
{
    qDebug()<<"修改自定义表数据操作,读userdefineTableCellUpdateBeforeInfo值："
           << "isDoubleClick = " << userDefineTableCellUpdateBeforeInfo.isDoubleClick
           << ",row = " << userDefineTableCellUpdateBeforeInfo.row
           << ",column = " << userDefineTableCellUpdateBeforeInfo.column
           << ",text = " << userDefineTableCellUpdateBeforeInfo.text;
    // 判断是否是界面进行了编辑操作，因为这个槽函数在表格不管是内容改变，行数量改变，代码操作单元格等都会触发
    // 所以不管是表格初始化，还是任何单元格有变化就会触发这个槽函数，而我们在单元格变化时不一定要进行修改操作
    // 这里需要阻断，我们只需要界面通过双击单元格后进行的操作才认为是修改操作
    // 所以判断是否进行双击单元格，判断双击的单元格行列是否等于现在改变单元格的行列，判断双击时老的单元格内容是否不等于改变时单元格的内容
    // 任何一个不满足，都直接进行返回，这么判断原因是首先要确认时双击时进行的判断，行列值保证双击时和修改时是同一个单元格，内容判断是
    // 保证用户可能双击单元格后并没有修改内容，这样该事件就不会触发，同时用户再可能将进行一些刷新列表什么的操作，这样该事件就会被反复触发
    // 而因为之前点击了单元格，所以是否双击单元格的bool值是否为true，和行列相等的条件在刷新到相同单元格内容时一定是满足条件的，通过内容是否不同，
    // 不同才进行修改，就可以阻断这种操作到一半不操作的情况，从而保证修改流程不出错
    if(!(userDefineTableCellUpdateBeforeInfo.isDoubleClick
         && row == userDefineTableCellUpdateBeforeInfo.row
         && column == userDefineTableCellUpdateBeforeInfo.column
         && userDefineTableCellUpdateBeforeInfo.text != ui->userdefineTable_tableWidget->item(row, column)->text())) {
        return;
    }
    // 进入阻塞模式，上面说到，很多情况会触发这个函数，如果不阻塞，处理方法中含有循环，判断等情况，因为数据相互干扰，可能发生程序死锁，导致崩溃
    // 这里阻塞后，那么表格所有的信号都会被阻塞，在本次处理结束前，就不会有其他情况进行干扰，避免死锁
    ui->userdefineTable_tableWidget->blockSignals(true);

    // 修改处理
    userDefineTableWidgetCellChangeUpdateTable(row, column);

    // 处理完成后解除阻塞模式
    ui->userdefineTable_tableWidget->blockSignals(false);

    // 修改操作完之后恢复原始的是否进行修改操作的标志信息，保证下一次修改流程的从头开始
    userDefineTableCellUpdateBeforeInfo.isDoubleClick = false;
    userDefineTableCellUpdateBeforeInfo.row = 0;
    userDefineTableCellUpdateBeforeInfo.column = 0;
    userDefineTableCellUpdateBeforeInfo.text = QString();
}

void columnSettingDialog::userDefineTableWidgetCellChangeUpdateTable(int row, int column)
{
    // 开启一个加载提示框
    LoadingDialog* loading = LoadingDialog::createLoading(this);

    qDebug()<<"进入修改自定义字段表数据操作";
    // 获取表格中修改的值
    QTableWidgetItem* item = ui->userdefineTable_tableWidget->item(row, column);
    QString text = item->text().trimmed();
    // 获取修改单元格对应的列头(中文名列头)
    QTableWidgetItem* horizontalHeaderItem = ui->userdefineTable_tableWidget->horizontalHeaderItem(column);
    QString columnZnName = horizontalHeaderItem->text(); // 列头中文名
    // 获得该列头在自定义字段表中的字段名称
    QString columnName = userDefineTableZnColumnNameAndColumnNames.value(columnZnName);
    // 从数据库中查询出该行对应的老数据
    int idColumnNum = TableWidgetOpt::findQTableWidgetIdZnNameColumn(ui->userdefineTable_tableWidget); // 获得修改行的id列所在索引值
    int nowRowId = ui->userdefineTable_tableWidget->item(row, idColumnNum)->text().toInt(); // 获得id列在该行的值
    QList<int> ids;
    ids.append(nowRowId); // 构造查询参数
    QList<UserDefineTableEntity> oldRowDatas = DataBaseOpt::selectUserDefineTableByIds(ids); // 查询出数据库对应数据
    UserDefineTableEntity oldUserDefineTableEntity = oldRowDatas.at(0); // 老数据（因为实际只能一次操作一行，根据id查询，id也只有一个，所以只有一个结果，直接获取即可）
    if(ObjectUtil::isEmpty(text)) {
        loading->close(); // 关闭加载提示框
        QMessageBox::information(this, "提示","填写内容不能为空");
        item->setText(getUserDefineEntityAttributeValue(columnZnName, oldUserDefineTableEntity)); // 恢复单元格未修改前的值
        return;
    }
    // 内容没有变化直接返回
    if(!userDefineTableCellContentIsChange(columnZnName, text, oldUserDefineTableEntity)) {
        loading->close(); // 关闭加载提示框
        item->setText(getUserDefineEntityAttributeValue(columnZnName, oldUserDefineTableEntity)); // 恢复单元格未修改前的值
        return;
    }
    // 进行页面单元格修改填写的信息正确性进行判断
    QList<QueryConditions> conditions;
    conditions.append(QueryConditions(columnName, text, false, "=", "and")); // 将修改列数据作为查询条件，后面会检查修改的数据是否存在
    // 表名称列如果不是英文字母和下划线的组合词直接返回提示错误信息
    if(columnZnName == UserDefineConstant::USERDEFINE_TABLE_COLUMN_TABLE_NAME_ZN_TITLE) {
        QRegExp rx("^[a-zA-Z_]+$");
        if(rx.indexIn(text) == -1){
            loading->close(); // 关闭加载提示框
            QMessageBox::information(this, "提示",columnZnName+"请填写英文字母和下划线的组合词");
            item->setText(getUserDefineEntityAttributeValue(columnZnName, oldUserDefineTableEntity)); // 恢复单元格未修改前的值
            return;
        }
        // 添加不等于自身老数据条件(排除自身原数据是因为修改可以为自身原来的数据，新数据才检查是否存在)
        conditions.append(QueryConditions(columnName, oldUserDefineTableEntity.tableName, false, "!=", "and"));
    }
    if(columnZnName == UserDefineConstant::USERDEFINE_TABLE_COLUMN_TABLE_ZN_NAME_ZN_TITLE) {
        // 添加不等于自身老数据条件(排除自身原数据是因为修改可以为自身原来的数据，新数据才检查是否存在)
        conditions.append(QueryConditions(columnName, oldUserDefineTableEntity.tableZnName, false, "!=", "and"));
    }
    // 数据库中如果已经存在了填写的表名称列或表中文名称列的内容，则直接返回提示错误信息
    int count = DataBaseOpt::selectAllCount(UserDefineConstant::USERDEFINE_TABLE_TABLE_NAME, conditions);
    if(count > 0) {
        loading->close(); // 关闭加载提示框
        QMessageBox::information(this, "提示",columnZnName+"已存在，请重新输入");
        item->setText(getUserDefineEntityAttributeValue(columnZnName, oldUserDefineTableEntity)); // 恢复单元格未修改前的值
        return;
    }

    if(DataBaseManage::getDb().driver()->hasFeature(QSqlDriver::Transactions)) { // 判断是否支持事务
        if(DataBaseManage::getDb().transaction()) { // 判断是否开启事务成功
            QList<bool> isSucess;
            // 更新数据库自定义表数据
            QMap<QString, QString> columnNameAndValues;
            columnNameAndValues.insert(columnName, text);
            isSucess.append(DataBaseOpt::updateTableById(UserDefineConstant::USERDEFINE_TABLE_TABLE_NAME, oldUserDefineTableEntity.id, columnNameAndValues));
            // 如果修改的是自定义表中表名字段数据，则修改具体表的表名
            if(columnZnName == UserDefineConstant::USERDEFINE_TABLE_COLUMN_TABLE_NAME_ZN_TITLE) {
                // 将真实的表的表名修改为最新的表名
                QMap<QString, QString> oldTableNameAndNewTableName;
                oldTableNameAndNewTableName.insert(oldUserDefineTableEntity.tableName, text);
                isSucess.append(DataBaseOpt::updateTableName(oldTableNameAndNewTableName)); // 修改具体表的表名

                // 将自定义字段表中的相应的关联表中的表名信息同步更新为最新的表名
                // 构造需要修改的列值（自定义字段表中关联表字段的值，修改为最新表名）
                QMap<QString, QString> columnNameAndValueMap;
                columnNameAndValueMap.insert(UserDefineConstant::TABLE_CONNECT_TABLE, text);
                // 构造查询条件（自定义字段表中关联表字段等于未修改前的表名的数据）
                QList<QueryConditions> columnConditions;
                columnConditions.append(QueryConditions(UserDefineConstant::TABLE_CONNECT_TABLE, oldUserDefineTableEntity.tableName, false, "=", "or"));
                isSucess.append(DataBaseOpt::updateTableDataByCondition(UserDefineConstant::COLUMN_TABLE_NAME, columnNameAndValueMap, columnConditions));
            }
            // 新的表中文名
            QString newTableZnName = oldUserDefineTableEntity.tableZnName;
            if(columnZnName == UserDefineConstant::USERDEFINE_TABLE_COLUMN_TABLE_ZN_NAME_ZN_TITLE) {
                newTableZnName = text;
            }

            // 没有出错则提交，出错则回滚
            if(isSucess.contains(false)) {
                item->setText(getUserDefineEntityAttributeValue(columnZnName, oldUserDefineTableEntity)); // 恢复单元格未修改前的值
                if(!DataBaseManage::getDb().rollback()) {  //回滚
                    qDebug()<< DataBaseManage::getDb().lastError();
                }
            } else {
                if(!DataBaseManage::getDb().commit()) {  //提交
                    qDebug()<< DataBaseManage::getDb().lastError();
                    if(!DataBaseManage::getDb().rollback()) {  //回滚
                        qDebug()<< DataBaseManage::getDb().lastError();
                    }
                }
            }
            loading->close(); // 关闭加载提示框
            // 成功刷新数据
            if(!isSucess.contains(false)) {
                // 刷新自定义表列表
                refreshUserDefineTableData();

                // 刷新表中文名下拉框数据
                refreshTableComboBox();

                // 刷新自定义字段表列表
                refreshCloumnTableData();

                // 刷新主界面左侧列表信息，和刷新主界面指定右侧tableName表格界面所有界面信息
                ((MainWindow* )UserDefineConstant::MAIN_WINDOW)->replaceAndRefreshClassifyListWidgetAndTableWidget(oldUserDefineTableEntity.tableZnName, newTableZnName);

                QMessageBox::information(this, "成功","修改成功");
            } else {
                item->setText(getUserDefineEntityAttributeValue(columnZnName, oldUserDefineTableEntity)); // 恢复单元格未修改前的值
                QMessageBox::information(this, "失败","修改失败");
            }
        } else {
            loading->close(); // 关闭加载提示框
            item->setText(getUserDefineEntityAttributeValue(columnZnName, oldUserDefineTableEntity)); // 恢复单元格未修改前的值
            QMessageBox::information(this, "失败","开启数据库事务失败，操作失败");
        }
    } else {
        loading->close(); // 关闭加载提示框
        item->setText(getUserDefineEntityAttributeValue(columnZnName, oldUserDefineTableEntity)); // 恢复单元格未修改前的值
        QMessageBox::information(this, "失败","不支持数据库事务，操作失败");
    }
}

bool columnSettingDialog::userDefineTableCellContentIsChange(QString columnZnName, QString text, UserDefineTableEntity userDefineTableEntity)
{
    // 内容没变化直接返回
    if(columnZnName == UserDefineConstant::USERDEFINE_TABLE_COLUMN_TABLE_NAME_ZN_TITLE && text == userDefineTableEntity.tableName) {
        return false;
    }
    if(columnZnName == UserDefineConstant::USERDEFINE_TABLE_COLUMN_TABLE_ZN_NAME_ZN_TITLE && text == userDefineTableEntity.tableZnName) {
        return false;
    }
    return true;
}

QString columnSettingDialog::getUserDefineEntityAttributeValue(QString columnZnName, UserDefineTableEntity userDefineTableEntity)
{
    // 根据列表头获取相对应的数据
    if(columnZnName == UserDefineConstant::USERDEFINE_TABLE_COLUMN_TABLE_NAME_ZN_TITLE) {
        return userDefineTableEntity.tableName;
    }
    if(columnZnName == UserDefineConstant::USERDEFINE_TABLE_COLUMN_TABLE_ZN_NAME_ZN_TITLE) {
        return userDefineTableEntity.tableZnName;
    }
    return QString();
}


