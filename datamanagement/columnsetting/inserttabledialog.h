#ifndef INSERTTABLEDIALOG_H
#define INSERTTABLEDIALOG_H

#include <QDialog>
#include <QString>
#include <QtWidgets>
#include <QSqlQuery>
#include <QSqlDriver>
#include <QSqlError>
#include "datamanagement/userdefine/userdefine.h"

namespace Ui {
class insertTableDialog;
}

class insertTableDialog : public QDialog
{
    Q_OBJECT

public:
    explicit insertTableDialog(QWidget *parent = 0);
    ~insertTableDialog();

private slots:
    void on_submit_pushButton_clicked();

private:
    Ui::insertTableDialog *ui;
    // 表名和表中文名映射
    QMap<QString, QString> tableZnNameAndTbaleNames;
    // 表中文名和组件（输入框/下拉框）的映射关系
    QMap<QString, QObject*> inputTextMap;
    // 界面布局
    void userDefinedlayout();
};

#endif // INSERTTABLEDIALOG_H
