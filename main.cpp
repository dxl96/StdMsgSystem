#include "datamanagement/mainwindows/mainwindow.h"
#include <QApplication>
#include "datamanagement/userdefine/userdefine.h"
#include "log/log.h"
#include <QtMessageHandler>

using namespace std;

void loadStyleSheet(); // 加载样式表函数声明
void loadSetting(); // 加载配置信息函数声明

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // 注册日志处理
    qInstallMessageHandler(Log::messageOutput);

    // 加载样式表
    loadStyleSheet();

    // 加载配置信息
    loadSetting();

    MainWindow* w = new MainWindow();

    // 设置主窗口
    UserDefineConstant::MAIN_WINDOW = w;
    w->show();

    return a.exec();
}

void loadStyleSheet() {
    // 加载样式表
    QFile styleSheet;
    styleSheet.setFileName(":/css/css/system.css");
//    styleSheet.setFileName("C:/Users/Administrator/Desktop/student_system/StdMgsystem/css/system.css");
    if(styleSheet.open(QFile::ReadOnly)) {
        QString styleString = styleSheet.readAll();
        styleSheet.close();
        qDebug()<<"样式表css文件内容："<<styleString;
        static_cast<QApplication*>(QApplication::instance())->setStyleSheet(styleString);
    }
}

void loadSetting() {
    QFile settingFile;
    settingFile.setFileName(":/setting/setting.json");
    if(settingFile.open(QFile::ReadOnly)) {
        // 读取json文件的内容，当前读取的内容可能是错误的，因为json文件不能有注释，而我在json中添加了注释，所以需要把注释内容去掉
        QByteArray setting = settingFile.readAll().trimmed();
        // 循环删除掉注释内容
        while (setting.contains("/*") && setting.contains("*/")) {
            int start = setting.indexOf("/*"); // 注释左索引（这里会返回左斜杠所在索引值）
            int end = setting.indexOf("*/") + 1; // 注释右索引（这里会返回*所在索引值，所以我们需要+1，将索引值切换到左斜杠位置）
            setting =  setting.remove(start, (end - start) + 1); // 移除 /* xxxx */ 这一块的注释内容，（结束索引-开始索引+1）是整个注释内容的长度
        }
        settingFile.close();
        qDebug()<<"配置json文件内容："<<QString(setting);
        QJsonParseError jsonError;
        QJsonDocument jsonDoc(QJsonDocument::fromJson(setting, &jsonError));
        if(jsonError.error == QJsonParseError::NoError)
        {
            QJsonObject rootObj = jsonDoc.object();
            // 下面调用toxxxx方法转换值时，传入方法的值是在没读取到相应属性时返回的默认值
            QString dataBaseDrive = rootObj.value("dataBaseDrive").toString(UserDefineConstant::DATA_BASE_DRIVE_NAME); // 读取配置文件中sql驱动的指定
            QJsonObject dataBaseNameParam = rootObj.value("dataBaseNameParam").toObject(QJsonObject()); // 读取配置文件中数据库名称设置参数
            QJsonArray dataBaseNames = dataBaseNameParam.value("names").toArray(QJsonArray()); // 读取配置文件中数据库名称集合
            int useIndex = dataBaseNameParam.value("useIndex").toInt(-1); // 读取配置文件中需要连接使用的数据库名称所在索引值
            QString dataBaseName = useIndex < 0 || useIndex > dataBaseNames.size() - 1
                    ? UserDefineConstant::DATA_BASE_NAME : dataBaseNames.at(useIndex).toString(UserDefineConstant::DATA_BASE_NAME); // 或取数据库名称
            QString dataBaseHostName = rootObj.value("dataBaseHostName").toString(UserDefineConstant::DATA_BASE_HOST_NAME); // 读取配置文件中数据库连接ip地址
            int dataBasePort = rootObj.value("dataBasePort").toInt(UserDefineConstant::DATA_BASE_PORT);  // 读取配置文件中数据库连接端口
            QString dataBaseUserName = rootObj.value("dataBaseUserName").toString(UserDefineConstant::DATA_BASE_USER_NAME); // 读取配置文件中数据库连接用户名
            QString dataBasePassWord = rootObj.value("dataBasePassWord").toString(UserDefineConstant::DATA_BASE_PASS_WORD); // 读取配置文件中数据库连接密码
            bool projectFirstStartLoadDefaultTableData = rootObj.value("projectFirstStartLoadDefaultTableData").toBool(UserDefineConstant::PROJECT_FIRST_START_LOAD_DEFAULT_TABLE_DATA); // 读取配置文件中工程第一次启动是否加载默认数据
            // 如果配置文件中的指定数据库既不是mysql也不是sqllite，则默认采用sqllite
            if(dataBaseDrive != UserDefineConstant::SQL_DRIVE_MYSQL_NAME && dataBaseDrive != UserDefineConstant::SQL_DRIVE_SQLLITE_NAME) {
                UserDefineConstant::DATA_BASE_DRIVE_NAME = UserDefineConstant::SQL_DRIVE_SQLLITE_NAME;
                qDebug()<<"读取到使用数据库类型："<<dataBaseDrive <<"不属于【mysql、sqllite】, 已切换到默认sqllite";
            } else {
                UserDefineConstant::DATA_BASE_DRIVE_NAME = dataBaseDrive;
                qDebug()<<"读取到使用数据库类型："<<dataBaseDrive;
            }
            qDebug()<<"读取到转换后数据库配置："
                   <<"dataBaseDrive:"+dataBaseDrive
                   <<"dataBaseName:"+dataBaseName
                   <<"dataBaseHostName:"+dataBaseHostName
                   <<"dataBasePort:"+QString::number(dataBasePort)
                   <<"dataBaseUserName:"+dataBaseUserName
                   <<"dataBasePassWord:"+dataBasePassWord;
            UserDefineConstant::DATA_BASE_NAME = dataBaseName; // 设置连接数据库名称
            // 以下数据库设置只对mysql生效
            UserDefineConstant::DATA_BASE_HOST_NAME = dataBaseHostName; // 设置连接数据库ip
            UserDefineConstant::DATA_BASE_PORT = dataBasePort; // 设置连接数据库端口
            UserDefineConstant::DATA_BASE_USER_NAME = dataBaseUserName; // 设置连接数据库用户名
            UserDefineConstant::DATA_BASE_PASS_WORD = dataBasePassWord; // 设置连接数据库密码
            // 设置第一次启动是否加载默认数据（当前默认数据为学生信息）
            UserDefineConstant::PROJECT_FIRST_START_LOAD_DEFAULT_TABLE_DATA = projectFirstStartLoadDefaultTableData;
            qDebug()<<"读取到项目是否启动默认数据："<<projectFirstStartLoadDefaultTableData;
        } else {
            qDebug() << "json error!" << jsonError.errorString();
        }
    }
}
