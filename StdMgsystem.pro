#-------------------------------------------------
#
# Project created by QtCreator 2018-07-07T17:42:02
#
#-------------------------------------------------

# 注：在该文件中所有指定引入的文件都是相对于本.pro文件的地址，也就是相对地址

QT       += core gui sql # 引入使用sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
CONFIG += resources_big # 指定资源文件可以超大资源
TARGET = StdMgsystem
TEMPLATE = app
OTHER_FILES += logo.rc # 在其他文件中添加logo.rc文件
RC_FILE += logo.rc # 指定rc文件，也就是通过该logo.rc文件，会将编译的可执行文件exe替换成含图标的样式
DEFINES += QT_MESSAGELOGCONTEXT # 日志

# 如何添加文件夹，请参考博文：https://blog.csdn.net/bdhjun123/article/details/80037653
# https://blog.csdn.net/weixin_39139505/article/details/102919398
# 一句话就是先在项目文件夹下新建好子文件，在用qt添加新文件时，目标路径选择在项目文件下已经存在的文件夹即可
# 如果在项目下已经存在了的文件，想挪位置，在将文件移动到了指定子文件夹后，在该.pro文件修改成对应路径即可，像下面这样，路径是相对路径（相对于.pro文件），根路径就是项目路径（也就是.pro所在路径），
# 最后指定的文件地址不要有右斜杠，否则qtcreater开发工具显示不出来最后一个文件，如果界面没显示，你也可以在qtcreater开发工具左侧项目页签上点击指定文件夹，选择添加已存在文件进行导入即可
# 最终所有文件的引入关系其实还是会在本文件中
# 同时记得将项目中.h和.cpp文件之间的引用到的相关头文件地址也改成下面这种对应的地址，ui文件中提升组件用到的类和头文件地址也记得改变，qtcreater开发工具会报警告和错误，也就是在头文件引用的地方会显示黄色下划线警告，修改即可
SOURCES += main.cpp\
        datamanagement/mainwindows/mainwindow.cpp \
        datamanagement/mainwindows/common/updatedialog.cpp \
	datamanagement/mainwindows/common/adddialog.cpp \
        datamanagement/mainwindows/common/informationdialog.cpp\
	datamanagement/mainwindows/common/commonform.cpp \
        datamanagement/columnsetting/columnsettingdialog.cpp \
        datamanagement/columnsetting/insaertcolumndialog.cpp \
        datamanagement/columnsetting/inserttabledialog.cpp \
        datamanagement/userdefine/userdefine.cpp \
        log/log.cpp \

HEADERS  += datamanagement/mainwindows/mainwindow.h \
        datamanagement/mainwindows/common/updatedialog.h \
	datamanagement/mainwindows/common/adddialog.h \
        datamanagement/mainwindows/common/informationdialog.h\
	datamanagement/mainwindows/common/commonform.h \
        datamanagement/columnsetting/columnsettingdialog.h \
        datamanagement/columnsetting/insaertcolumndialog.h \
        datamanagement/columnsetting/inserttabledialog.h \
        datamanagement/userdefine/userdefine.h \
        log/log.h \

FORMS    += datamanagement/mainwindows/mainwindow.ui \
        datamanagement/mainwindows/common/updatedialog.ui \
	datamanagement/mainwindows/common/adddialog.ui \
        datamanagement/mainwindows/common/informationdialog.ui \
	datamanagement/mainwindows/common/commonform.ui \
        datamanagement/columnsetting/columnsettingdialog.ui \
        datamanagement/columnsetting/insaertcolumndialog.ui \
        datamanagement/columnsetting/inserttabledialog.ui \

RESOURCES += \
    resouce.qrc

DISTFILES += \
    logo.rc
